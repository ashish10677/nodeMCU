def hello_world():
    import network
    import socket
    import machine
    
    def log(msg):
        print(msg)
    
    def start_myserver():
        LED0 = machine.Pin(2, machine.Pin.OUT)
        log('start server method')
        addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
        s = socket.socket()
        s.bind(addr)
        s.listen(1)
        log('listening on'+str( addr))
        while True:
            cl, addr = s.accept()
            log('client connected from'+str(addr))
            request = cl.recv(1024)
            request = str(request)
            LEDON0 = request.find('/?LED=ON0')
            LEDOFF0 = request.find('/?LED=OFF0')
            print("Content = %s" % str(request))
            if LEDON0 == 6:
                print('TURN LED0 ON')
                LED0.off()
            if LEDOFF0 == 6:
                print('TURN LED0 ON')
                LED0.on()
            cl.send(html)
            cl.close()
    
    #main part
    html = b"""<!DOCTYPE html>
    <html>
        <head> <title>ESP8266</title> </head>
        <body> 
            Hello from Python web on ESP8266
            <form>
            <button name="LED" id = "on" value = "ON0">On</button>
            <button name="LED" id = "off" value = "OFF0">Off</button>
            </form>
        </body>
    </html>
    """
    start_myserver()